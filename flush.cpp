//Test for the cache line flush instruction : clflush

//Original code from : http://stackoverflow.com/questions/20600798/clflush-not-flushing-the-instruction-cache

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define SIZE 1024 //Size of int array to fill
#define TESTS 16 //Number of times to repeat experiment

//Clear Cache line at address
inline void
clflush(volatile void *p)
{
    asm volatile ("clflush (%0)" :: "r"(p));
}

//Read Time-Stamp Counter
inline uint64_t
rdtsc()
{
    unsigned long a, d;
    asm volatile ("cpuid; rdtsc" : "=a" (a), "=d" (d) : : "ebx", "ecx");
    return a | ((uint64_t)d << 32);
}

inline int func(int *A) {
  int sum = 0;
  for (int i=0;i<SIZE;++i) {
    sum += A[i];
  }
  return sum;
}

inline uint64_t test(int *A)
{
    uint64_t start, end;
    char c;
    start = rdtsc();
    func(A);
    end = rdtsc();
    return end-start;
}

void flushCache(int *A)
{
  for (int i=0;i<SIZE;++i) {
    clflush(&A[i]);
  }
}

int main(int ac, char **av)
{
  for (int t=0;t<TESTS;++t) {

    int A[SIZE];
    for (int i=0;i<SIZE;++i) {
      A[i] = i;
    }

    uint64_t ticks1, ticks2;
    ticks1 = test(A);
    printf("Run 1: %ld ticks\n",ticks1);
    ticks2 = test(A);
    printf("Run 2: %ld\n",ticks2);

    flushCache(A);
    printf("Cache Flushed\n");

    ticks1 = test(A);
    printf("Run 3: %ld (d=%ld)\n",ticks1,ticks1-ticks2);
    ticks2 = test(A);
    printf("Run 4: %ld (d=%ld)\n",ticks2,ticks2-ticks1);
    printf("-----------------\n");
  }
  return 0;
}
